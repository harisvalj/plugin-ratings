<?php
   /*
   Plugin Name: Product ratings plugin
   Description: Plugin for Product ratings by Haris
   Version: 1.0.2
   Author: Haris Valjevcic
   */
   ?>
   
   <?php

   function style_css() {
    $plugin_url = plugin_dir_url( __FILE__ );

    wp_enqueue_style( 'style1', $plugin_url . '/assets/css/style.css' );
}
add_action( 'wp_enqueue_scripts', 'style_css' );
   function create_product_post_type() {
   	register_post_type( 'Product',
   		array(
   			'labels' => array(
   				'name' => __( 'Products' ),
   				'singular_name' => __( 'Product' )
   			),
   			'public' => true,
   			'has_archive' => true,
   			'supports' => array(),
   			'taxonomies' => array('target_groups')
   		)
   	);
   }
   add_action( 'init', 'create_product_post_type' );


   class Products_widget extends WP_Widget {

	// Main constructor of class
   	public function __construct() {
   		parent::__construct(
   			'my_custom_widget',
   			__( 'Products Widget', 'text_domain' ),
   			array(
   				'customize_selective_refresh' => true,
   			)
   		);
   	}

	// The widget form (for the backend )
   	public function form( $instance ) {	

	// Set widget defaults
   		$defaults = array(
   			'title'    => '',
   			'text'     => '',
   			'textarea' => '',
   			'checkbox' => '',
   			'select'   => '',
   		);

	// Parse current settings with defaults
   		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

   		<?php // Widget Title ?>
   		<p>
   			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'text_domain' ); ?></label>
   			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
   		</p>

   	<?php }

	// Update widget settings
   	public function update( $new_instance, $old_instance ) {
   		$instance = $old_instance;
   		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
   		$instance['text']     = isset( $new_instance['text'] ) ? wp_strip_all_tags( $new_instance['text'] ) : '';
   		$instance['textarea'] = isset( $new_instance['textarea'] ) ? wp_kses_post( $new_instance['textarea'] ) : '';
   		$instance['checkbox'] = isset( $new_instance['checkbox'] ) ? 1 : false;
   		$instance['select']   = isset( $new_instance['select'] ) ? wp_strip_all_tags( $new_instance['select'] ) : '';
   		return $instance;
   	}

	// Display the widget
   	public function widget( $args, $instance ) {

   		extract( $args );

	// Check the widget options
   		$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
   		$text     = isset( $instance['text'] ) ? $instance['text'] : '';
   		$textarea = isset( $instance['textarea'] ) ?$instance['textarea'] : '';
   		$select   = isset( $instance['select'] ) ? $instance['select'] : '';
   		$checkbox = ! empty( $instance['checkbox'] ) ? $instance['checkbox'] : false;

	// WordPress core before_widget hook (always include )
   		echo $before_widget;

   // Display the widget
   		echo '<div class="widget-text wp_widget_plugin_box">';

		// Display widget title if defined
   		if ( $title ) {
   			echo $before_title . $title . $after_title;
   			echo '<hr>';
   		}

		// Args for loop
   		$args = array(
   			'post_type'   => 'product',
   			'post_status' => 'publish',
   			'meta_key'			=> 'stars',
			'orderby'			=> 'meta_value',
			'order'				=> 'DESC'
   		);


		//Displaying products into widget 
   		$products = new WP_Query( $args );
   		if( $products->have_posts() ) :
   			?>
   			<ul>
   				<?php
   				while( $products->have_posts() ) :
   					$products->the_post();
   					?>
   					<img src="<?php echo get_field('image'); ?> " class="image">
   					<?php 
   					$starsNum = get_field('stars');
   					switch ($starsNum) {
   						case 1:
   						echo '<div><span class="stars-container stars-1">★★★★★</span></div>';
   						break;
   						case 2:
   						echo '<div><span class="stars-container stars-2">★★★★★</span></div>';
   						break;
   						case 3:
   						echo '<div><span class="stars-container stars-3">★★★★★</span></div>';
   						break;
   						case 4:
   						echo '<div><span class="stars-container stars-4">★★★★★</span></div>';
   						break;
   						case 5:
   						echo '<div><span class="stars-container stars-5">★★★★★</span></div>';
   						break;

   						default:

   						break;
   					}
   					?>
   					<h3><?php echo the_title(); ?></h3>

   					<?php
   				endwhile;
   				wp_reset_postdata();
   				?>
   			</ul>
   			<?php
   		else :
   			esc_html_e( 'No products!', 'text-domain' );
   		endif;

   		echo '</div>';

	// WordPress core after_widget hook (always include )
   		echo $after_widget;

   	}

   }

// Register the widget
   function product_widget() {
   	register_widget( 'Products_widget' );
   }
   add_action( 'widgets_init', 'product_widget' );


   //hook into the init action and call create_book_taxonomies when it fires
	add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
	 
	//create a custom taxonomy name it topics for your posts
	 
	function create_topics_hierarchical_taxonomy() {
	 
	// Add new taxonomy, make it hierarchical like categories
	//first do the translations part for GUI
	 
	  $labels = array(
	    'name' => _x( 'Target Groups', 'taxonomy general name' ),
	    'singular_name' => _x( 'Target group', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Search Target groups' ),
	    'all_items' => __( 'All Target groups' ),
	    'parent_item' => __( 'Parent target group' ),
	    'parent_item_colon' => __( 'Parent target group:' ),
	    'edit_item' => __( 'Edit target group' ), 
	    'update_item' => __( 'Update target group' ),
	    'add_new_item' => __( 'Add New target group' ),
	    'new_item_name' => __( 'New target group Name' ),
	    'menu_name' => __( 'Target groups' ),
	  );    
	 
	// Now register the taxonomy
	 
	  register_taxonomy('target_groups',array('post'), array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'topic' ),
	  ));
	 
	}

	/* Settings */
	function products_settings_page() {
   add_option( 'myplugin_option_name', 'This is my option value.');
   register_setting( 'myplugin_options_group', 'myplugin_option_name', 'myplugin_callback' );
}
add_action( 'admin_init', 'products_settings_page' );
function myplugin_register_options_page() {
  add_options_page('Page Title', 'Plugin Menu', 'manage_options', 'myplugin', 'myplugin_options_page');
}
add_action('admin_menu', 'myplugin_register_options_page');
function myplugin_options_page()
{
?>
  <div>
  <?php screen_icon(); ?>
  <h2>My Plugin Page Title</h2>
  <form method="post" action="options.php">
  <?php settings_fields( 'myplugin_options_group' ); ?>
  <h3>This is my option</h3>
  <p>Some text here.</p>
  <table>
  <tr valign="top">
  <th scope="row"><label for="myplugin_option_name">Label</label></th>
  <td><input type="text" id="myplugin_option_name" name="myplugin_option_name" value="<?php echo get_option('myplugin_option_name'); ?>" /></td>
  </tr>
  </table>
  <?php  submit_button(); ?>
  </form>
  </div>
<?php
} ?>
   ?>